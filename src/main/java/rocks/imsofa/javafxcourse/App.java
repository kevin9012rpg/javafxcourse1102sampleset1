package rocks.imsofa.javafxcourse;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {

    @Override
    public void start(Stage stage) {
        BorderPane borderPane=new BorderPane();
        FlowPane topPane=new FlowPane();
        FlowPane bottomPane=new FlowPane();
        borderPane.setTop(topPane);
        borderPane.setBottom(bottomPane);
        Button button1=new Button("Open");
        Button button2=new Button("Colse");
        topPane.getChildren().addAll(button1, button2);
        
        Label label=new Label("......");
        bottomPane.getChildren().add(label);
        
        AnchorPane anchorPane=new AnchorPane();
        borderPane.setLeft(anchorPane);
        Button button3=new Button("Display");
        button3.setPrefHeight(30);
        button3.setPrefWidth(100);
        AnchorPane.setBottomAnchor(button3, 5.0);
        AnchorPane.setLeftAnchor(button3, 5.0);
        AnchorPane.setRightAnchor(button3, 5.0);
        anchorPane.getChildren().add(button3);
        
        ListView list=new ListView();
        list.setItems(FXCollections.observableArrayList("Option 1", "Option 2", "Option 3"));
        AnchorPane.setTopAnchor(list, 5.0);
        AnchorPane.setBottomAnchor(list, 40.0);
        AnchorPane.setLeftAnchor(list, 5.0);
        AnchorPane.setRightAnchor(list, 5.0);
        anchorPane.getChildren().add(list);
        
        Scene scene = new Scene(borderPane, 640, 480);
        stage.setScene(scene);
        stage.show();
        
    }

    public static void main(String[] args) {
        launch();
    }

}